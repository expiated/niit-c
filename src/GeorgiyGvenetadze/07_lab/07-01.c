#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*
�������� ���������, ��������� ��������� ������ � �������� �
�������� � �� ����� � ������������ � ����������� ����� ������
���������:
���� ����������� �� ������: http: //introcs.cs.princeton.edu/java/data/fips10_4.csv

��������� ������ ������������ ��������� �������:
(a) ������������ ������ �� ������ ������ �����.
(b) ����� � ����� ���� ������ �� ���������� ����������� ������.
(c) ����� ����������� ������� �� ��������.
*/

struct NAME_REC 
{
	char iso[3];
	char fips[3];
	char name[50];
};

typedef struct NAME_REC TNAME_REC; 
typedef TNAME_REC * PNAME_REC;

struct ITEM 
{
	PNAME_REC name_rec;
	struct ITEM *next;
	struct ITEM *prev;
};

typedef struct ITEM TITEM;
typedef TITEM * PITEM;
PITEM createList(PNAME_REC name_rec);
PNAME_REC creatName(char *line); 
PITEM addToTail(PITEM tail,PNAME_REC name_rec);
int countList(PITEM head);
PITEM findByIso(PITEM head,char *name);
PITEM findByName(PITEM head,char *city);
void printAllData(PITEM item);

int main( )
{
	FILE *fp;
	int count=0;
	char buf[512];
	char code_menu[3];
	char search[50];

	PITEM head,tail,item;
	fp=fopen("fips10_4.csv","rt");
	if(!fp)
	{
		perror("File names.csv:");
		exit(1);
	}
	fgets(buf,512,fp);
	while(fgets(buf,512,fp))
	{
		if(count==0)
		{
			head=createList(creatName(buf));
			tail=head;
		}
		else
		{
			tail=addToTail(tail,creatName(buf));
		}
		count++;
	}
	fclose(fp);

	printf("Total items: %d\n\n",countList(head));
	
	puts("Please select search:");
	puts("1 - search by letter designation of the country (ISO)");
	puts("2 - search in the region title (city name)");
	fgets(code_menu,3,stdin);

	if(code_menu[0]=='1')
	{
		//(b) ����� � ����� ���� ������ �� ���������� ����������� ������.
		puts("\nPlease enter ISO-3166 country (for example RU):");
		fgets(search,3,stdin);
		item=findByIso(head,search);
		if(item==NULL)
			printf("Not found!\n");
		else
			printAllData(item);
	}
	else if(code_menu[0]=='2')
	{
		//(c) ����� ����������� ������� �� ��������.
		puts("\nPlease enter the city name in quotes (for example \"Moskva\"):");
		fgets(search,50,stdin);
		item=findByName(head,search);
		if(item==NULL)
			printf("Not found!\n");
		else
			printAllData(item);
	} else
		puts("This menu does not exist. Please enter again.");

	return 0;
}

PITEM createList(PNAME_REC name_rec)
{
	PITEM item=(PITEM)malloc(sizeof(TITEM));
	item->name_rec=name_rec;
	item->prev=NULL;
	item->next=NULL;
	return item;
}

PNAME_REC creatName(char *line)
{
	int i=0;
	PNAME_REC rec=(PNAME_REC)malloc(sizeof(TNAME_REC));

	while(*line && *line!=',')
		rec->iso[i++]=*line++;

	rec->iso[i]=0;
	line++;
	i=0;

	while(*line && *line!=',')
		rec->fips[i++]=*line++;

	rec->fips[i]=0;
	line++;
	i=0;

	while(*line)
		rec->name[i++]=*line++;
	rec->name[i]=0;
	return rec;
}

PITEM addToTail(PITEM tail,PNAME_REC name_rec)
{
	PITEM item=createList(name_rec);
	if(tail!=NULL)
	{
		tail->next=item;
		item->prev=tail;
	}
	return item;
}

int countList(PITEM head)
{
	int count=0;
	while(head)
	{
		count++;
		head=head->next;
	}
	return count;
}

PITEM findByIso(PITEM head,char *iso_country)
{
	while(head)
	{
		if(strcmp(head->name_rec->iso,iso_country)==0)
			return head;
		head=head->next;
	}
	return NULL;
}

PITEM findByName(PITEM head,char *city)
{
	while(head)
	{
		if(strcmp(head->name_rec->name,city)==0)
			return head;
		head=head->next;
	}
	return NULL;
}

void printAllData(PITEM item)
{
	if(item!=NULL)
	{
		printf("ISO 3166 country - ");
		puts(item->name_rec->iso);
		printf("FIPS 10-4 region code - ");
		puts(item->name_rec->fips);
		printf("Name city- ");
		puts(item->name_rec->name);
	}
}