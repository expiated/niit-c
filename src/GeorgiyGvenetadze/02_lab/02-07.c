#include <stdio.h>
#include <string.h>

#define SIZE 256
/*
�������� ���������, ��������� ������� ������������� �������� ��� ������-
��� ������������� ������. � ���� ������� ���������� ������ ������ � �����
��� ����������.
���������:
� ���� ��������� �� ��������� � �������� �������, ��� ��� �������������
�������������� �������� ���������.*/int main()
{
	char user_string[SIZE] = { 0 };
	int counter[SIZE] = { 0 };

	printf("Enter your string (255 chars max):\n");
	fgets(user_string, SIZE, stdin);

	int i;
	for (i = 0; i < strlen(user_string) - 1; i++)
		counter[user_string[i]]++;

	for (int i = 0; i < SIZE; i++)
		if (counter[i] != '\0' && counter[i] != '\n')
			printf("%c - %d\n", i, counter[i]);

	return 0;
}