#include <stdio.h>

#define I_MAX 80
#define J_MAX 80
/*
�������� ���������, ��������� �� ����� ����������� �� ��������
���������:
����������� ������ ��������� ���:
  *
 ***
*****
���������� ����� ������� ������������� � ����������
*/

int main()
{
	int ln;
	printf("Enter the number of rows:\n");
	scanf_s("%d", &ln);

	int row, spc, astrsk;

	for (row = 1; row <= ln; row++)
	{
		for (spc = ln - row; spc >= 1; spc--)
			printf(" ");
		for (astrsk = 1; astrsk <= row * 2 - 1; astrsk++)
			printf("*");
		printf("\n");
	}
	return 0;
}