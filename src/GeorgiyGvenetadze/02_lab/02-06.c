#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define SIZE 256
#define IN 1	// these variables define the counter condition: if condition is IN - counter is in the word,
#define OUT 0	// if counter is OUT - counter is out of the word.

/*
�������� ���������, ��������� ������ �� ������ ��������. ������� ���-
������ ������� � ������ ������, � ����� ������ � ������� ����� �������,
���� �� ���������� ������ 1.
���������:
� ������ ��������� ��������� ��������� �������������� �������, �� ����
���������� ���������� � �������� ������. ����� ���������� ��������� ���-
����� �� �����.
*/

int main()
{
	char user_str[SIZE] = { 0 };
	int condition = OUT;

	printf("Enter your message:\n");
	fgets(user_str, SIZE, stdin);

	int i = 0;
	while (i < strlen(user_str))
	{
		if (!isalnum(user_str[i]) && condition == OUT)
			for (int j = i; j < strlen(user_str); j++)
				user_str[j] = user_str[j + 1];

		else if (!isalnum(user_str[i]) && condition == IN)
		{
			if (user_str[i] == '\n')
				user_str[i++] = user_str[i + 1];
			else
			{
				user_str[i] = ' ';
				condition = OUT;
				i++;
			}
		}
		else if (isalnum(user_str[i]) && condition == OUT)
			condition = IN;
		else if (isalnum(user_str[i]) && condition == IN)
			i++;
	}

	puts(user_str);

	return 0;
}