#include <stdio.h>
#include <ctype.h>

#define SIZE 13

/*
�������� ���������, �������������� ������� � ������� �������� �������:
������� ���� ��������� �����, ����� �����. ������ �������� � ���� �������-
�� � ���� ��������� ������������������ ���� � ����. ������������ �������-
�������� ��������� ������.
���������:
���������� � ������ ������ ����������� ����� �� ������������. ����� ��-
���������� ����������� ��������� �������
*/

void swap(char* arg1, char* arg2);

int main()
{
	char str[SIZE] = "b1eu2tif4u4l";

	int i = 0, j = SIZE - 2;
	while (i < j)
	{
		if (isdigit(str[i]))
			swap(&str[i], &str[j--]);
		else
			i++;
	}
	printf("%s\n", str);

	return 0;
}

void swap(char* arg1, char* arg2)
{
	char temp = *arg1;
	*arg1 = *arg2;
	*arg2 = temp;
}