#include <stdio.h>
#include <math.h>
#include <windows.h>

#define G 9.81

/*�������� ���������, ����������� ������ ���������� �����. ���-
�� ������ � ������ H, ������� �������� �������������. � �����
������ ������� ����� ������ ���������� ���������� �� �������
L =gt2/2, ��� g = 9.81.
��������� ����� ����������� ��� � ������� � ������� �� �����-
��� ������� �������� ������ ��� ������������ ����� h.
���������:
t=00 c h=5000.0 �
t=01 c h=4995.4 �
...
t=31� h=0234.8 �
BABAH!!!*/void finalCounter(float arg1);int main(){	float altitude = 0;	printf("Enter the altitude value (float), which the bomb will fall: \n");	scanf_s("%f", &altitude);	finalCounter(altitude);	return 0;}void finalCounter(float altitude){	for (int sec = 0; altitude > 0; sec++)	{		altitude -= G*pow(sec, 2) / 2;		if (altitude < 0)			break;		printf("t=%02d c h=%06.1f m\n", sec, altitude);		Sleep(1000);	}	printf("VAVAN!!!\n");}