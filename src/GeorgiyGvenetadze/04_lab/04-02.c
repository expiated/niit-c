#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define SIZE 256
#define IN 1
#define OUT 0

/* �������� ���������, ������� � ������� ������� ���������� ������� �����
������ � �������� �������
���������:
������ �� ������� ��������� ������ ���������� �� char, � ������� ���������
������ ������ �������� ������� ����� (������������ - ������ ������� � ��-
�������� ��������). ����� �� ���������� ����� ����� ������, ��������� ����
������ �� ����������. */

int main()
{
	char user_str[SIZE] = { 0 };
	char *pointer[SIZE];

	int condition = OUT;

	fgets(user_str, SIZE, stdin);

	int i = 0, word_count = 0;
	while (i < strlen(user_str))
	{
		if (!isspace(user_str[i]) && condition == OUT)
		{
			condition = IN;
			pointer[word_count++] = user_str + i;
		}
		else if (isspace(user_str[i]) && condition == IN)
			condition = OUT;
		i++;
	}

	
	while (word_count > 0)
	{
		char *p;
		p = pointer[--word_count];
		while (*p != ' ' && 
			   *p != '\0')
			putchar(*p++);
		putchar(' ');
	}

	return 0;
}