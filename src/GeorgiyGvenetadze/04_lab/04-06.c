#include <stdio.h>

#define SIZE 256

/* �������� ���������, ������� ����������� ���������� ������������� � �����,
� ����� ��������� ������ ��� ������������ � ��� �������. ��������� ������
���������� ������ �������� � ������ ������� ������������
���������:
����� ������� ������ ����� ��� �������� ��� � ��� ���������: young � old,
������� �� ���� �����, ��������� � ������� ��������. */

int main()
{
	int relative_cnt = 0;
	int youngest = 0;
	int oldest = 0;

	char names[SIZE][SIZE] = { 0 };
	char *young, *old;

	printf("How many persons are in your family?\n");
	scanf_s("%d", &relative_cnt);
	fflush(stdin);

	printf("List all family members:\n");
	for (int i = 0; i < relative_cnt; i++)
	{
		int age = 0;

		printf("Name of %i person - ", i + 1);	// I wanted to do it in one line
		fgets(names[i], SIZE, stdin);			// by scanf_s, but garbage that remained 
		printf("Age of %i person - ", i + 1);	// in stdin run endless cycle
		scanf_s("%i", &age);
		fflush(stdin);

		if (i == 0)
			youngest = age;

		if (age < youngest)
		{
			youngest = age;
			young = names[i];
		}
		else if (age > oldest)
		{
			oldest = age;
			old = names[i];
		}
	}

	printf("The youngest person of your family is "); puts(young);
	printf("The oldest person of your family is "); puts(old);

	return 0;
}