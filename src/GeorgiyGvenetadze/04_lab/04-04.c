#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define SIZE 256
#define IN 1
#define OUT 0

/*
�������� ��������� ��� ������ ����� ������� ������������������� � ���-
���� � �������������� ���������� ������ �������� ����������.
���������:
���������� ����� �������� ������ ���� �������� � �������������� ��������
���, ����� ������ � ������ ������������� ����� ���������.

�������� ���������, ������������ � ������ ���������� ������������������
������������ �����
���������:
��� ������ AABCCCDDEEEEF ��������� 4 � EEEE
*/

int main()
{
	char user_str[SIZE] = "AABCCCDDEEEEF";
	char *parsed[SIZE];
	int sequence = OUT;

	int char_count = 0, row_count = 0, i = 0, max_sequence = 0;
	while (i < strlen(user_str))
	{
		if (isalnum(user_str[i]) && sequence == OUT)
		{
			parsed[row_count] = &user_str[i];
			sequence = IN;
			char_count++;
			i++;
		}
		else if (isalnum(user_str[i]) && sequence == IN)
		{
			if (user_str[i] == user_str[i - 1])
			{
				char_count++;
				i++;
			}
			else
			{
				if (strlen(parsed[row_count]) > max_sequence)
					max_sequence = row_count;
				char_count = 0;
				sequence = OUT;
				parsed[++row_count] = &user_str[i];
			}
		}
		else
			i++;
	}

	char *pl = parsed[max_sequence];
	char *pr = parsed[max_sequence + 1];
	i = 0;

	while (pl < pr)
	{
		putchar(*pl++);
		i++;
	}
	printf(" - %i chars\n", i);

	return 0;
}