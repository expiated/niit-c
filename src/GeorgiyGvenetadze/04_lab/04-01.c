#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 256

/* �������� ���������, ������� ��������� ������������ ������ �������-
�� ����� � ����������, � ����� ��������� �� � ������� ��������-
��� ����� ������.
���������:
������ �������� �� ��������� ������ ������ � ������������ � ���������
���������� ������. ������������ ���������� �������� � ���������� �������
���������� �� char. ����� ��������� ����� ��������� ��������� �������� �
������� � ������� ������ � ������������ � ���������������� �����������. */

int compare(const char **a, const char **b);

int main()
{
	char user_strs[SIZE][SIZE] = { 0 };
	char *pointer[SIZE];
	
	int array_size_counter = 0;
	for (array_size_counter = 0; fgets(user_strs[array_size_counter], SIZE, stdin) != NULL 
		&& user_strs[array_size_counter][0] != '\n'; array_size_counter++)
		pointer[array_size_counter] = user_strs[array_size_counter];

	qsort(pointer, array_size_counter, sizeof(char *), compare);

	for (int i = 0; i < array_size_counter; i++)
		printf("%s", pointer[i]);

	return 0;
}

int compare(const char **a, const char **b)
{
 	int a_len = strlen(*a);
	int b_len = strlen(*b);
	return (a_len > b_len) - (a_len < b_len);
}