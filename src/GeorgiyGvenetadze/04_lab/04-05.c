#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 256

/* �������� ���������, ����������� ������ (��. ������ 1), �� ������������
������, ����������� �� ���������� �����. ��������� ������ ��������� �����
������������ � ����. */

int compare(const char **a, const char **b);

int main()
{
	FILE *file;
	char *filename = "input_data_set.txt";

	char user_strs[SIZE][SIZE] = { 0 };
	char *pointer[SIZE];

	file = fopen(filename, "r");

	if (file == NULL)
	{
		printf("Can't open file\n");
		return 0;
	}

	int array_size_counter = 0;
	for (array_size_counter = 0; fgets(user_strs[array_size_counter], SIZE, file) != NULL
		&& user_strs[array_size_counter][0] != '\n'; array_size_counter++)
		pointer[array_size_counter] = user_strs[array_size_counter];

	fclose(file);

	qsort(pointer, array_size_counter, sizeof(char *), compare);

	char *filename2 = "output_data_set.txt";
	file = fopen(filename2, "w+");

	for (int i = 0; i < array_size_counter; i++)
		fprintf(file, "%s", pointer[i]);
	
	printf("Your strings are sorted and stored to the file.\n");

	fclose(file);

	return 0;
}

int compare(const char **a, const char **b)
{
	int a_len = strlen(*a);
	int b_len = strlen(*b);
	return (a_len > b_len) - (a_len < b_len);
}