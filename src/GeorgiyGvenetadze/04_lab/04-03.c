#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define SIZE 256
#define YES 1
#define NO 0

/* �������� ���������, ������� ����������� ������ � ����������, �� ��������
�� ������ ����������� (��������� �������� � ����� ������� � ������ ������)
���������:
���� ������ - ��������� ��������� ��� �������� ������������ ������ � ����
������ */

int main()
{
	char user_str[SIZE] = { 0 };
	char buff[SIZE] = { 0 };
	int isPalindrome = YES;

	fgets(user_str, SIZE, stdin);
	user_str[strlen(user_str) - 1] = '\0';
	strcpy_s(buff, sizeof(user_str), user_str);

	for (int j = 0; j < strlen(user_str); j++)
		user_str[j] = tolower(buff[j]);

	int i = 0;
	while (i < strlen(user_str))
	{
		if (isspace(user_str[i]) || ispunct(user_str[i]))
			for (int j = i; j < strlen(user_str); j++)
				user_str[j] = user_str[j + 1];
		else
			i++;
	}

	char *pi, *pj;
	pi = &user_str[0];
	pj = &user_str[strlen(user_str) - 1];

	while (pj >= pi)
	{
		if (*pi != *pj)
			isPalindrome = NO;
		pi++; pj--;
	}

	if (isPalindrome)
		printf("\"%s\" is a palindrome!\n", buff);
	else
		printf("\"%s\" is not a palindrome!\n", buff);

	return 0;
}