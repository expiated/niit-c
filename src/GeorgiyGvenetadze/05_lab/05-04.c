#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#define SIZE 256
#define IN 1
#define OUT 0

/*
�������� ���������, ������� ������ ��������� ��������� ���� �
������������ �������� ����� � ������ ������
���������:
��������� ��������� ������������ ���������� ���� � ������ ��� �������-
��. ��� ������ ������ ���������� �������, ������������� � ������ ������
1.
*/

void parseString(char str[], char *pstr[]);

void getWord(char *pstr[], int position);

void shuffle(char *pstr[]);

int element_counter = 0;

int main()
{
	FILE *pFile;
	char *filename = "input_data.txt";

	char user_str[SIZE][SIZE] = { 0 };
	char *parsed_file[SIZE];
	char *parsed_str[SIZE];

	srand(time(NULL));

	pFile = fopen(filename, "r");
	if (pFile == NULL)
	{
		printf("Can't open file\n");
		return 0;
	}

	int file_str_counter = 0;
	while (!feof(pFile))
	{
		fgets(user_str[file_str_counter], SIZE, pFile);
		parsed_file[file_str_counter] = user_str[file_str_counter];
		file_str_counter++;
	}
	fclose(pFile);

	for (int i = 0; i < file_str_counter; i++)
	{
		parseString(parsed_file[i], parsed_str);
		shuffle(parsed_str);
		putchar('\n');
	}

	return 0;
}

void parseString(char str[], char *pstr[])
{
	int condition = OUT;
	int i = 0, j = 0;

	while (i < strlen(str))
	{
		if (isalnum(str[i]) && condition == OUT)
		{
			condition = IN;
			pstr[j++] = &str[i];
			element_counter++;
		}
		else if (!isalnum(str[i]) && condition == IN)
		{
			condition = OUT;
			i++;
		}
		else
			i++;
	}
}

void getWord(char *pstr[], int position)
{
	char *pl = pstr[position];
	while (isalnum(*pl))
		putchar(*pl++);
	putchar(' ');
}

void shuffle(char *pstr[])
{
	int arr[SIZE] = { 0 };

	for (int i = 0; i < element_counter; i++)
	{
		arr[i] = rand() % element_counter + 0 - 0;
		for (int j = 0; j < i; j++)
		{
			if (arr[j] == arr[i])
			{
				i--;
				break;
			}
		}
	}
	for (int i = 0; i < element_counter; i++)
		getWord(pstr, arr[i]);
	element_counter = 0;
}
