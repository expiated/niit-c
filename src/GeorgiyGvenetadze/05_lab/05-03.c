#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#define SIZE 256
#define IN 1
#define OUT 0

/*
�������� ���������, �������������� ��������� ������� �����-
�� ������� ����� ������ ������ ���������� �����, ����� �������
� ����������, �� ���� ������ � ����� ����� �������� �� ������.
���������:
��������� ��������� ������������ ���������� ���� � ������ ��� �������-
��. ��� ������ ������ ����������� �������� �� ����� � ����������� ����-
����� ������� �����
*/

void parseString(char str[], char *pstr[]);

void getWord(char *pstr[], int position);

void printLine(char *pstr[]);

void swap(char *p1, char *p2);

int strLength(char *p);

int element_counter = 0;

int main()
{
	FILE *pFile;
	char *filename = "input_data.txt";

	char user_str[SIZE][SIZE] = { 0 };
	char *parsed_file[SIZE];
	char *parsed_str[SIZE];

	srand(time(NULL));

	pFile = fopen(filename, "r");
	if (pFile == NULL)
	{
		printf("Can't open file\n");
		return 0;
	}

	int file_str_counter = 0;
	while (!feof(pFile))
	{
		fgets(user_str[file_str_counter], SIZE, pFile);
		parsed_file[file_str_counter] = user_str[file_str_counter];
		file_str_counter++;
	}
	fclose(pFile);

	for (int i = 0; i < file_str_counter; i++)
	{
		parseString(parsed_file[i], parsed_str);
		printLine(parsed_str);
		putchar('\n');
	}

	return 0;
}

void parseString(char str[], char *pstr[])
{
	int condition = OUT;
	int i = 0, j = 0;

	while (i < strlen(str))
	{
		if (isalnum(str[i]) && condition == OUT)
		{
			condition = IN;
			pstr[j++] = &str[i];
			element_counter++;
		}
		else if (!isalnum(str[i]) && condition == IN)
		{
			condition = OUT;
			i++;
		}
		else
			i++;
	}
}

void getWord(char *pstr[], int position)
{
	char *p = pstr[position];
	int wordLength = strLength(p);

	if (wordLength - 1 > 1)
	for (int i = 0; i < wordLength; i++)
	{
		int randomLetter = (i + 1) + rand() % (wordLength - 1) - (i + 1);
		if (randomLetter < 1)
		{
			i--;
			continue;
		}
		if (p + i < p + wordLength - 1)
			swap(p + 1, p + randomLetter);
	}

	while (isalnum(*p))
		putchar(*p++);
	putchar(' ');
}

void printLine(char *pstr[])
{
	for (int i = 0; i < element_counter; i++)
		getWord(pstr, i);
	element_counter = 0;
}

void swap(char *p1, char *p2)
{
	char pTemp;
	pTemp = *p1;
	*p1 = *p2;
	*p2 = pTemp;
}

int strLength(char *p)
{
	int count = 0;
	while (isalnum(*p))
	{
		count++;
		p++;
	}
	return count;
}