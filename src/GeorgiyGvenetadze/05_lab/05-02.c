#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <Windows.h>

#define ROWS 20
#define COLS 40
 
/*
�������� ��������� ������������, ��������� �� ����� ������-
�����, ������������ �� ����������� ������������� ��������� �*�.
����������� ����������� � ��������� ���������� �������, � ��-
��� ��� ����� � ����������� ���������� � ��������� ��� �����.
���������:
������� ������ ��������� � ���� ��������� ������������������ �����:
1) ������� ������� (���������� ���������)
2) ������������ ��������� ������� �������� ������ ��������� (���������
�*�)
3) ����������� �������� � ������ ��������� �������
4) ������� ������
5) ����� ������� �� ����� (���������)
6) ��������� ��������
7) ������� � ���� 1.
*/

void clearTheArray (int rows, int cols, char arr[ROWS][COLS]);

void drawThePattern (int rows, int cols, char arr[ROWS][COLS]);
 
int main()
{
    srand(time(NULL));
    char kaleidoscope[ROWS][COLS] = { 0 };

    while(1)
    {
		printf("Ctrl+C if you are tired\n\n");
        clearTheArray (ROWS, COLS, kaleidoscope);
        drawThePattern (ROWS, COLS, kaleidoscope);
        Sleep(1000);
		system("cls");
    }

	return 0;
}
 
void clearTheArray (int rows, int cols, char arr[ROWS][COLS])
{
    for (int i = 0; i < rows; i++)
        for (int j = 0; j < cols; j++)
            arr[i][j] = ' ';
}
 
void drawThePattern (int rows, int cols, char arr[ROWS][COLS])
{
    int astx = 0;
    for (int i = 0; i < rows / 2; i++)
        for (int j = 0; j < cols / 2; j++)
        {
            astx = rand() % 2;
            if (astx == 1)
                arr[i][j] = '*';
        }
 
    for (int i = 0; i < rows / 2; i++)
        for (int j = 0; j < cols / 2; j++)
            if (arr[i][j] == '*')
            {
                arr[i][(cols - 1) - j] = '*';
                arr[(rows - 1) - i][j] = '*';
                arr[(rows - 1) - i][(cols - 1) - j] = '*';
            }
 
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
            putchar(arr[i][j]);
        putchar('\n');
    }
}