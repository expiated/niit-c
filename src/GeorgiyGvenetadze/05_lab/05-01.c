#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#define SIZE 256
#define IN 1
#define OUT 0

/*
�������� ���������, ������� ��������� �� ������������ ������ �
������� �� �� �����, ��������� ����� � ��������� �������.
���������:
��������� ������ �������� ������� �� ���� �������:
a) printWord - ������� ����� �� ������ (�� ����� ������ ��� �������)
b) getWords - ��������� ������ ���������� �������� ������ ���� ����
c) main - �������� �������
*/

void parseString(char str[], char *pstr[]);

void getWord(char *pstr[], int position);

void shuffle(char *pstr[]);

int element_counter = 0;

int main()
{
	char user_str[SIZE] = { 0 };
	char *parsed[SIZE];

	srand(time(NULL));

	fgets(user_str, SIZE, stdin);

	parseString(user_str, parsed);
	shuffle(parsed);

	return 0;
}

void parseString(char str[], char *pstr[])
{
	int condition = OUT;
	int i = 0, j = 0;

	while (i < strlen(str))
	{
		if (isalnum(str[i]) && condition == OUT)
		{
			condition = IN;
			pstr[j++] = &str[i];
			element_counter++;
		}
		else if (!isalnum(str[i]) && condition == IN)
		{
			condition = OUT;
			i++;
		}
		else
			i++;
	}
}

void getWord(char *pstr[], int position)
{
	char *pl = pstr[position];
	while (isalnum(*pl))
		putchar(*pl++);
	putchar(' ');
}

void shuffle(char *pstr[])
{
	int arr[SIZE] = { 0 };

	for (int i = 0; i < element_counter; i++)
	{
		arr[i] = rand() % element_counter + 0 - 0;
		for (int j = 0; j < i; j++)
		{
			if (arr[j] == arr[i])
			{
				i--;
				break;
			}
		}
	}
	for (int i = 0; i < element_counter; i++)
		getWord(pstr, arr[i]);
}
