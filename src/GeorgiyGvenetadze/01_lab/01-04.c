#include <stdio.h>

#define INCH 2.54

/*�������� ���������, ������� ��������� ���� �� ������������ ��-
����� (����, �����) � ����������� (����������). ������ ���-
����� � ���� ���� ����� �����, ��������� � ���� �������������
����� � ��������� �� 1 �����. 1 ��� = 12 ������. 1 ���� = 2.54
��.*/

void metricConverter(int arg1, int arg2);

int main()
{
	int ft, inches;

	printf("Enter your height in pounds and dimes:\n");
	scanf_s("%i%i", &ft, &inches);

	metricConverter(ft, inches);

	return 0;
}

void metricConverter(int ft, int inches)
{
	float result;
	result = ft*12*INCH + inches*INCH;

	printf("%i ft %i inches = %.1f cm\n", ft, inches, result);
}