#include <stdio.h>

#define PI 3.14

/*�������� ���������, ������� ��������� �������� ���� �� �����-
��� � �������, �, ��������, � ����������� �� ������� ��� �����.
��������: 45.00D - �������� �������� � ��������, � 45.00R - �
��������. ���� ������ �������������� �� ������� %f%c*/

void angleConverter(float arg1, char arg2);

int main()
{
	char typeOfAngle;
	float unit;

	printf("Enter the current value and choose your angle type by the D or R postfix (f.e. 45.00D):\n");
	scanf_s("%f%c", &unit, &typeOfAngle);
	angleConverter(unit, typeOfAngle);

	return 0;
}

void angleConverter(float unit, char typeOfAngle)
{
	float result;
	if (typeOfAngle == 'R')
	{
		result = unit*(180 / PI);
		printf("%.2f rads are %.2f degrees\n", unit, result);
	}
	else if (typeOfAngle == 'D')
	{
		result = unit*(PI / 180);
		printf("%.2f degrees are %.2f rads\n", unit, result);
	}
}