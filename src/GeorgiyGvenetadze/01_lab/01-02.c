#include <stdio.h>

/*�������� ���������, ������� ����������� ������� ����� � ���-
���� ��:��:��, � ����� ������� ����������� � ����������� ��
���������� ������� ("������ ���� "������ ����"� �.�.)*/

void greetings(int arg1);

int main()
{
	int hour, minute, second;

	printf("Enter the current time HH:MM:SS\n");
	scanf_s("%i:%i:%i", &hour, &minute, &second);

	greetings(hour);

	return 0;
}

void greetings(int hour)
{
	if (hour > 4 && hour < 11)
		printf("Good morning!\n");
	else if (hour > 11 && hour < 17)
		printf("Good afternoon!\n");
	else if (hour > 17 && hour < 23)
		printf("Good evening!\n");
	else if (hour > 23 || hour < 4)
		printf("Good night!\n");
}