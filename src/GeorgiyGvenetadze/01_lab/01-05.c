#include <stdio.h>
#include <string.h>

#define LINE 80

/*�������� ���������, ������� ��������� ������ �� ������������ �
������� � �� �����, ���������� �� ������*/

void centerAndDisplay(char* arg1);

int main()
{
	char userStr[LINE];
	printf("Enter your data pls:\n\n");
	fgets(userStr, LINE, stdin);
	centerAndDisplay(userStr);

	return 0;
}

void centerAndDisplay(char* str)
{
	int len = strlen(str);
	int spc = (LINE - len) / 2;

	if (len%2 == 0)
		printf("%*s", spc + len, str);
	else
		printf("%*s", spc + len + 1, str);
}