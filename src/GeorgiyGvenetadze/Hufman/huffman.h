#include <stdio.h>

struct SYM
{
	unsigned char ch;
	float freq;
	char code[256];
	struct SYM *left;
	struct SYM *right;
};

typedef struct SYM SYM;

union CODE {
	unsigned char ch;
	struct {
		unsigned short b1 : 1;
		unsigned short b2 : 1;
		unsigned short b3 : 1;
		unsigned short b4 : 1;
		unsigned short b5 : 1;
		unsigned short b6 : 1;
		unsigned short b7 : 1;
		unsigned short b8 : 1;
	} byte;
};

union CODE code;

SYM* makeTree(SYM**, int);
void writeHeader(FILE *fp, SYM *SymArr, char charCounter, int tailSize);
int readHeader(FILE *fp, int *charCounter, struct SYM *psym);
void makeCodes(SYM*);
void tableOfFrequencies(FILE *fp, SYM *SymArr, int *uniqueCharsCounter, int *charsQuantity, int *charCounter);
void charFrequienceCounter(SYM *SymArr, SYM **psym, int *uniqueCharsCounter, int *charsQuantity, int *charCounter);
void descendingSort(SYM *SymArr, int *charCounter);
void printTable(SYM *SymArr, SYM **psym, int *charCounter, float *freqAmount, int *charsQuantity);
void creatingBinaryFile(FILE *fp, FILE *fp2, SYM *SymArr, int charCounter);
int binaryFileSizeCalculation(FILE *fp);
unsigned char pack(unsigned char buf[]);
void storeToFreqTable(FILE *fp, int charCounter, SYM *SymArr);
void packing(FILE *fp, FILE *fp2, int tempCharQuantity, int tailSize);
void readFile(fp, fp2, tailSize);
unsigned char SearchCode(FILE* fp, struct SYM *SymArr);