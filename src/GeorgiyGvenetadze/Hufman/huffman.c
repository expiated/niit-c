#define _CRT_SECURE_NO_WARNINGS

#include <string.h>
#include <stdlib.h>
#include "huffman.h"

SYM *makeTree(struct SYM *psym[], int k)
{
	SYM *temp;
	int i, j;
	temp = (SYM*)malloc(sizeof(SYM));
	temp->freq = psym[k - 2]->freq + psym[k - 1]->freq;
	temp->left = psym[k - 1];
	temp->right = psym[k - 2];
	temp->code[0] = 0;

	if (k <= 2)
		return temp;
	else
	{
		for (i = 0; i<k; i++)
			if (temp->freq > psym[i]->freq)
			{
				for (j = k - 1; j>i; j--)
					psym[j] = psym[j - 1];

				psym[i] = temp;
				break;
			}
	}
	return makeTree(psym, k - 1);
}

void writeHeader(FILE *fp, SYM *psym, char charCounter, int tailSize)
{
	fwrite("compression", sizeof(char), 11, fp);
	fwrite(&charCounter, sizeof(int), 1, fp);
	for (int i = 0; i<charCounter; i++)
	{
		fwrite(&psym[i].ch, sizeof(SYM), 1, fp);
		fwrite(&psym[i].freq, sizeof(SYM), 1, fp);
	}
	fwrite(&tailSize, sizeof(int), 1, fp);
}

int readHeader(FILE *fp, int *charCounter, struct SYM *SymArr)
{
	char signature[11];
	char chtail, ch;
	int i, tail;
	fread(&signature, sizeof(char), 11, fp);
	fread(&ch, sizeof(char), 1, fp);
	*charCounter = (int)ch;
	for (i = 0; i < *charCounter; i++)
	{
		fread(&SymArr[i].ch, sizeof(char), 1, fp);
		fread(&SymArr[i].freq, sizeof(float), 1, fp);
	}
	fread(&chtail, sizeof(char), 1, fp);
	tail = (int)chtail;
	return tail;
}

void makeCodes(SYM *root)
{
	if (root->left)
	{
		strcpy(root->left->code, root->code);
		strcat(root->left->code, "0");
		makeCodes(root->left);
	}
	if (root->right)
	{
		strcpy(root->right->code, root->code);
		strcat(root->right->code, "1");
		makeCodes(root->right);
	}
}

void tableOfFrequencies(FILE *fp, SYM *SymArr, int *uniqueCharsCounter, int *charsQuantity, int *charCounter)
{
	int ch = 0;
	while ((ch = fgetc(fp)) != EOF)
	{
		for (int j = 0; j<256; j++)
		{
			if (ch == SymArr[j].ch)
			{
				uniqueCharsCounter[j]++;
				(*charsQuantity)++;
				break;
			}
			if (SymArr[j].ch == 0)
			{
				SymArr[j].ch = (unsigned char)ch;
				uniqueCharsCounter[j] = 1;
				(*charCounter)++; (*charsQuantity)++;
				break;
			}
		}
	}
}

void charFrequienceCounter(SYM *SymArr, SYM **psym, int *uniqueCharsCounter, int *charsQuantity, int *charCounter)
{
	for (int i = 0; i<*charCounter; i++)
		SymArr[i].freq = (float)uniqueCharsCounter[i] / *charsQuantity;

	for (int i = 0; i<*charCounter; i++)
		psym[i] = &SymArr[i];
}

void descendingSort(SYM *SymArr, int *charCounter)
{
	SYM SymTemp;
	for (int i = 1; i<*charCounter; i++)
		for (int j = 0; j<*charCounter - 1; j++)
			if (SymArr[j].freq<SymArr[j + 1].freq)
			{
				SymTemp = SymArr[j];
				SymArr[j] = SymArr[j + 1];
				SymArr[j + 1] = SymTemp;
			}
}

void printTable(SYM *SymArr, SYM **psym, int *charCounter, float *freqAmount, int *charsQuantity)
{
	for (int i = 0; i<*charCounter; i++)
	{
		*freqAmount += SymArr[i].freq;
		printf("%3d\tCh = %3d\tFrequencie = %f\tChar = %c\t\n", i, SymArr[i].ch, SymArr[i].freq, psym[i]->ch);
	}
	printf("\nSymbols = %d\tAmount of frequencie = %f\t letters = %d\n", *charsQuantity, *freqAmount, *charCounter);
}

void creatingBinaryFile(FILE *fp, FILE *fp2, SYM *SymArr, int charCounter)
{
	int ch = 0;
	while ((ch = fgetc(fp)) != EOF)
	{
		for (int i = 0; i<charCounter; i++)
			if (ch == SymArr[i].ch)
				fputs(SymArr[i].code, fp2);
	}
}

int binaryFileSizeCalculation(FILE *fp)
{
	int tempCharQuantity = 0, ch = 0;
	while ((ch = fgetc(fp)) != EOF)
		tempCharQuantity++;
	return tempCharQuantity;
}

unsigned char pack(unsigned char buf[])
{
	code.byte.b1 = buf[0] - '0';
	code.byte.b2 = buf[1] - '0';
	code.byte.b3 = buf[2] - '0';
	code.byte.b4 = buf[3] - '0';
	code.byte.b5 = buf[4] - '0';
	code.byte.b6 = buf[5] - '0';
	code.byte.b7 = buf[6] - '0';
	code.byte.b8 = buf[7] - '0';
	return code.ch;
}

void packing(FILE *fp, FILE *fp2, int tempCharQuantity, int tailSize)
{
	char buf[8];
	int i, j = 0;
	for (i = 0; i < (tempCharQuantity - tailSize); i++) 
	{
		buf[j] = getc(fp);
		if (j == 7)
		{
			fputc(pack(buf), fp2);
			j = 0;
		}
		else j++;
	}
	if (tailSize != 0) 
	{
		for (i = 0; i < 8; i++)
			buf[i] = 0;
		j = 0;

		while (j < tailSize)
		{
			buf[j] = getc(fp);
			j++;
		}
		fputc(pack(buf), fp2);
	}
}

void readFile(fp, fp2, tailSize)
{
	char buf[8], decoder;
	int symbol, i;

	decoder = fgetc(fp2);
	while ((symbol = fgetc(fp2)) != EOF)
	{
		code.ch = decoder;
		fputc(code.byte.b1 + '0', fp);
		fputc(code.byte.b2 + '0', fp);
		fputc(code.byte.b3 + '0', fp);
		fputc(code.byte.b4 + '0', fp);
		fputc(code.byte.b5 + '0', fp);
		fputc(code.byte.b6 + '0', fp);
		fputc(code.byte.b7 + '0', fp);
		fputc(code.byte.b8 + '0', fp);
		decoder = symbol;
	}
	code.ch = decoder;
	buf[0] = code.byte.b1 + '0';
	buf[1] = code.byte.b2 + '0';
	buf[2] = code.byte.b3 + '0';
	buf[3] = code.byte.b4 + '0';
	buf[4] = code.byte.b5 + '0';
	buf[5] = code.byte.b6 + '0';
	buf[6] = code.byte.b7 + '0';
	buf[7] = code.byte.b8 + '0';
	for (i = 0; i < tailSize; i++)
		fputc(buf[i], fp);
}

unsigned char SearchCode(FILE* fp, struct SYM *SymArr)
{
	int ch = -1;
	if (ch == '0' && SymArr->left != NULL && SymArr->right != NULL)
		return SearchCode(fp, SymArr->left);
	if (ch == '1' && SymArr->left != NULL && SymArr->right != NULL)
		return SearchCode(fp, SymArr->right);
	if (SymArr->left == NULL && SymArr->right == NULL)
		ungetc(ch, fp);
	return SymArr->ch;
}