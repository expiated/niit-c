#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include "huffman.h"

int main(int argc, char *argv[])
{
	int ans = 2;
	if (ans == 1)
	{
		FILE *fpIn, *fpTemp, *fpOut;

		int charCounter = 0;
		int charsQuantity = 0;
		int tempCharQuantity = 0;
		int tailSize;
		int uniqueCharsCounter[256] = { 0 };
		SYM SymArr[256] = { 0 };
		SYM *psym[256] = { 0 };
		float freqAmount = 0;

		fpIn = fopen("war_peace.txt", "rb");
		fpTemp = fopen("temp.101", "wb");
		fpOut = fopen("war_peace.txt.arh", "wb");

		if (fpIn == NULL)
		{
			puts("ERROR! FILE CAN'T BE OPEN");
			return -1;
		}

		tableOfFrequencies(fpIn, SymArr, uniqueCharsCounter, &charsQuantity, &charCounter);
		charFrequienceCounter(SymArr, psym, uniqueCharsCounter, &charsQuantity, &charCounter);
		descendingSort(SymArr, &charCounter);
		printTable(SymArr, psym, &charCounter, &freqAmount, &charsQuantity);

		SYM *root = makeTree(psym, charCounter);

		makeCodes(root);

		rewind(fpIn);
		creatingBinaryFile(fpIn, fpTemp, SymArr, charCounter);

		fclose(fpIn);
		fclose(fpTemp);

		fpTemp = fopen("temp.101", "rb");
		tempCharQuantity = binaryFileSizeCalculation(fpTemp);
		tailSize = tempCharQuantity % 8;
		writeHeader(fpOut, psym, charCounter, tailSize);

		rewind(fpTemp);
		packing(fpTemp, fpOut, tempCharQuantity, tailSize);

		fclose(fpIn);
		fclose(fpTemp);
		fclose(fpOut);

		return 0;
	}
	if (ans == 2)
	{
		/* decompressor */
		FILE *fpIn, *fpTemp, *fpOut;

		SYM SymArr[256] = { 0 };
		SYM *psym[256] = { 0 };
		int charCounter = 0;
		int charsQuantity = 0;
		int tailSize = 0;
	
		fpIn = fopen("war_peace.txt.arh", "rb");
		fpTemp = fopen("temp.101", "wb");

		tailSize = readHeader(fpIn, &charCounter, SymArr);
		readFile(fpTemp, fpIn, tailSize);

		fclose(fpIn);
		fclose(fpTemp);

		for (int i = 0; SymArr[i].ch != '\0'; i++)
			psym[i] = &SymArr[i];

		SYM *root = makeTree(psym, charCounter);
		makeCodes(root);

		fpTemp = fopen("temp.101", "rb");
		fpOut = fopen("yourFile.txt", "wb");

		printf("Done!\n");
		int ch;
		while ((ch = fgetc(fpTemp)) != EOF)
			fputc(SearchCode(fpTemp, root), fpOut);

		printf("Done!\n");
		fclose(fpIn);
		fclose(fpTemp);
		fclose(fpOut);

		return 0;
	}
	return 0;
}