#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE 80

/* �������� ���������, ������� ��������� ������������� ������ ������� N,
� ����� ������� ����� ��������� ����� ����������� � ������������ ���-
�������. */

int compare(const void * a, const void * b);

int main()
{
	srand(time(NULL));

	int arr[SIZE] = { 0 };

	for (int i = 0; i < SIZE; i++)
		arr[i] = rand() % (SIZE / 2 - -SIZE / 2 + 1) + -SIZE / 2;

	qsort(arr, SIZE, sizeof(int), compare);

	printf("amouint of the first and the last values eqeals %d\n", arr[0] + arr[SIZE - 1]);

	return 0;
}

int compare(const void * a, const void * b)
{
	return (*(int*)a - *(int*)b);
}