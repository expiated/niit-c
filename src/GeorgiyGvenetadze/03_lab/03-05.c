#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE 80

/*  �������� ���������, ������� ��������� ������������� ������ ������� N, �
����� ������� ����� ���������, ������������� ����� ������ ����������-
��� � ��������� ������������� ����������.
���������:
������ ����������� ���������� �������: �������������� � ������������-
�� ������� (��� ����� �������...)	*/

int compare(const void * a, const void * b);

int main()
{
	srand(time(NULL));

	int arr[SIZE] = { 0 };
	int amount = 0;

	for (int i = 0; i < SIZE; i++)
		arr[i] = rand() % (SIZE/2 - -SIZE/2 + 1) + -SIZE/2;

	qsort(arr, SIZE, sizeof(int), compare);

	for (int i = 1; i < SIZE - 1; i++)
		amount += arr[i];

	printf("amouint eqeals - %d\n", amount);

	return 0;
}

int compare(const void * a, const void * b)
{
	return (*(int*)a - *(int*)b);
}