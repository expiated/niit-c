#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define SIZE 256
#define IN 1
#define OUT 0

/* �������� ���������, ������������ � ������ ���������� ������������������
������������ �����
���������:
��� ������ AABCCCDDEEEEF ��������� 4 � EEEE */

int main()
{
	char user_str[SIZE] = "AABCCCDDEEEEF";
	char parsed[SIZE][SIZE] = { 0 };
	int sequence = OUT;

	int char_count = 0, row_count = 0, i = 0, max_sequence = 0;
	while (i < strlen(user_str))
	{
		if (isalnum(user_str[i]) && sequence == OUT)
		{
			parsed[row_count][char_count++] = user_str[i];
			sequence = IN;
			i++;
		}
		else if (isalnum(user_str[i]) && sequence == IN)
		{
			if (user_str[i] == user_str[i - 1])
			{
				parsed[row_count][char_count++] = user_str[i];
				i++;
			}
			else
			{
				if (strlen(parsed[row_count]) > max_sequence)
					max_sequence = row_count;
				char_count = 0;
				sequence = OUT;
				parsed[++row_count][char_count] = user_str[i];
			}
		}
		else
			i++;
	}

	printf("%d - %s\n", strlen(parsed[max_sequence]), parsed[max_sequence]);

	return 0;
}