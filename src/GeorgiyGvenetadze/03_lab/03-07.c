#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define SIZE 256

/* �������� ���������, ������� �������� ������� ������������� �������� ���
�������� ������, ��������������� �� �������� �������
���������:
������� ��������� ����� �������, ��� ������� ���� ����� ��������������
�������, � ����� �� ���� ��������. */

int compare(const void *a, const void *b);

int main()
{
	char user_string[SIZE] = { 0 };
	int counter_table[SIZE][2] = { 0 };

	printf("Enter your string (255 chars max):\n");
	fgets(user_string, SIZE, stdin);

	for (int i = 0; i < strlen(user_string) - 1; i++)
	{
		counter_table[user_string[i]][0] = user_string[i];
		counter_table[user_string[i]][1]++;
	}

	qsort(counter_table, SIZE, sizeof(int)* 2, compare);

	for (int i = SIZE - 1; counter_table[i][0] != 0; i--)
		if (counter_table[i][0] != '\0' && counter_table[i][0] != '\n')
			printf("%c - %d\n", counter_table[i][0], counter_table[i][1]);

	return 0;
}

int compare(const void *a, const void *b)
{
	return ((const int *)a)[1] - ((const int *)b)[1];
}
