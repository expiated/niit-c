#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define SIZE 256
#define IN 1	
#define OUT 0

char parsed[SIZE][SIZE] = { 0 };

/* �������� ���������, ������� ����������� � ������������ ������, ���������
�� ���������� ���� � ����� ����� n, � ����� ������� n - �� ����� �� ������. �
������ ������������� n ��������� ��������� �� ������
���������:
� ���������� ������ ��������� ������� ������ ���� ������� �� �������.
��������� �������������� ������� ������ */

int main()
{
	char user_str[SIZE] = { 0 };
	int numberOfWord = 0;
	int condition = OUT;

	printf("Enter your message:\n");
	fgets(user_str, SIZE, stdin);	scanf_s("%d", &numberOfWord);	int i = 0, row_cnt = 0, char_cnt = 0;	while (i < strlen(user_str))	{
		if (!isalnum(user_str[i]) && condition == IN)
		{
			condition = OUT;
			char_cnt = 0;
			row_cnt++;
			i++;
		}		else if (isalnum(user_str[i]) && condition == OUT)
			condition = IN;
		else if (isalnum(user_str[i]) && condition == IN)
		{
			parsed[row_cnt][char_cnt] = user_str[i];
			char_cnt++;
			i++;
		}
		else if (!isalnum(user_str[i]) && condition == OUT)
			i++;	}

	if (numberOfWord <= 0 || numberOfWord > row_cnt)
		printf("There is no such a word!\n");
	else
		for (int l = 0; l < row_cnt; l++)
			if (l != numberOfWord - 1)
			{
				for (int k = 0; parsed[l][k] != '\0'; k++)
					putchar(parsed[l][k]);
				printf(" ");
			}

	puts("");

	return 0;
}