#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define SIZE 256
#define MAX_RANK 6
#define IN 1
#define OUT 0

/* �������� ���������, ������� ������� ����� ����� �� �������� ������
���������:
��������� ������������� ����������� ������������������ ���� � ������ ���
����� � ������������ �� ��� ������ �����. � ��������� ������������� ������-
����� �� ������������ ����� ��������, �� ���� ���� ������������ ������ �����
������� ������������������ ����, � ����� ������� �� ��������� �����. */

int main()
{
	char user_str[SIZE] = "1234567 1 21 23 sad 123456 123";
	char parsed[SIZE][SIZE] = { 0 };
	int condition = 0;
	int amount = 0;

	int char_count = 0, row_count = 0, user_str_char_counter = 0, max = 0;
	while (user_str_char_counter < strlen(user_str))
	{
		if (isdigit(user_str[user_str_char_counter]) && max < MAX_RANK && condition == IN)
		{
			parsed[row_count][char_count++] = user_str[user_str_char_counter];
			max++;
			user_str_char_counter++;
		}
		else if (isdigit(user_str[user_str_char_counter]) && max == MAX_RANK && condition == IN)
		{
			char_count = 0;
			max = 0;
			row_count++;
		}
		else if (!isdigit(user_str[user_str_char_counter]) && condition == IN)
		{
			condition = OUT;
			char_count = 0;
			max = 0;
			row_count++;
		}
		else if (isdigit(user_str[user_str_char_counter]) && condition == OUT)
			condition = IN;
		else
			user_str_char_counter++;
	}

	for (int i = 0; parsed[i][0] != 0; i++)
		amount += atoi(parsed[i]);

	printf("Amount is %d\n", amount);

	return 0;
}