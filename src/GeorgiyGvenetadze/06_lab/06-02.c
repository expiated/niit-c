﻿#include <stdio.h>

#define SIZE 1000000

/*
Написать программу, которая находит в диапазоне целых чисел от
2 до 1000000 число, формирующее самую длинную последователь-
ность Коллатца
Замечание:
Последовательностью Коллатца называют числовой ряд, каждый элемент ко-
торого формируется в зависимости от чётности/ нечётности предыдущего по
закону:
n → 3n + 1, если n нечётное.
n → n/2, если n чётное.
Формирование последовательности начинается с некоторого n (в задаче от 2
до 1000000), а заканчивается, когда n становится равным 1. Необходимо найти
такое исходное n, которое формирует самую длинную последовательность.
*/

void collatzSequence(long long n);

int sequenceCounter = 0;

int main()
{
	int seq = 0, number = 0;

	for (int i = 2; i < SIZE; i++)
	{
		sequenceCounter = 0;
		collatzSequence(i);
		if (sequenceCounter > seq)
		{
			seq = sequenceCounter;
			number = i + 1;
		}
	}

	printf("The longest sequense is %i from the %i number\n", seq, number);

	return 0;
}

void collatzSequence(long long n)
{
	sequenceCounter++;
	if (n != 1)
		if (n % 2 == 1)
			collatzSequence(3 * n + 1);
		else
			collatzSequence(n / 2);
}