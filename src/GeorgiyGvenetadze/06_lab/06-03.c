#include <stdio.h>
#include <string.h>

#define SIZE 256

/*
�������� ���������, ������� ��������� �������� �������������
����� ����� � ������ � �������������� �������� � ��� �����-����
������������ ������� ��������������
*/

void intToString(int n, char *p);

void revertString(char *p);

void swap(char *p1, char *p2);

char str[SIZE] = { 0 };

int main()
{
	char *p = str;
	int usernum = 0;
	
	scanf_s("%i", &usernum);

	intToString(usernum, p);
	revertString(str);
	printf("Now \"%s\" is a string\n", str);

	return 0;
}

void intToString(int n, char *p)
{
	if (n > 0)
	{
		*p = n % 10 + 48;
		intToString(n / 10, p + 1);
	}
}

void revertString(char *p)
{
	for (int i = 0, j = strlen(str) - 1; p + i < p + j; i++, j--)
		swap(p + i, p + j);
}

void swap(char *p1, char *p2)
{
	char temp;
	temp = *p1;
	*p1 = *p2;
	*p2 = temp;
}