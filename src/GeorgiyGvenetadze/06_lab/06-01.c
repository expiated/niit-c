#include <stdio.h>
#include <math.h>

#define SIZE 40

/*
�������� ���������, ������� ��������� � ��������� ����������
������� ����������� ����������� � ������� ��� �� ������� (��.
�������)
���������:
��������� ������ �������� �����������, ����� ����� �������� ������� ��-
����. �������, ��� ��������� �������� ���������� �������� � ���������
���������� �����������, ������������ ������ �����������.
*/

void clearTheArray(int rows, int cols);

void makeThePattern(int level, int rows, int cols);

void draw(int rows, int cols);

char area[SIZE][SIZE] = { 0 };

int main()
{
	clearTheArray(SIZE, SIZE);
	makeThePattern(3, SIZE / 2, SIZE / 2);
	draw(SIZE, SIZE);

	return 0;
}

void clearTheArray(int rows, int cols)
{
	for (int i = 0; i < rows; i++)
		for (int j = 0; j < cols; j++)
			area[i][j] = ' ';
}

void makeThePattern(int level, int rows, int cols)
{
	int shift = (int)pow(3, level - 1);
	if (level == 0)
		area[rows][cols] = '*';
	else
	{
		makeThePattern(level - 1, rows, cols);
		makeThePattern(level - 1, rows - shift, cols);
		makeThePattern(level - 1, rows + shift, cols);
		makeThePattern(level - 1, rows, cols - shift);
		makeThePattern(level - 1, rows, cols + shift);
	}
}

void draw(int rows, int cols)
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < cols; j++)
			putchar(area[i][j]);
		putchar('\n');
	}
}