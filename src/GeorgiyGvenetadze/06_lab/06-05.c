#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <time.h>

/*
�������� ���������, ������� �������� ����� ���������� N-���
����� ���� ���������. ������������� ����� ������� ��������
��� N � ��������� �� 1 �� 40 (��� � ������ ��������� �� �������)
�� ����� � � ����
���������:
��������� ���� �� ���������� ����� ������� � ����������� ������� � ��-
������� ������ ����������� ������� �� ����� ���� N
*/

long fibonacciSequence(long n);

int main()
{
	FILE *pFile;
	char *filename = "fibonacci_table.csv";

	pFile = fopen(filename, "w+");
	if (pFile == NULL)
	{
		printf("Can't open file\n");
		return 0;
	}

	long number = 0;
	long result = 0;
	clock_t time;
	double timeTaken;

	scanf_s("%li", &number);

	fprintf(pFile, "NUMBER, TIME, RESULT\n");

	for (int i = 1; i <= number; i++)
	{
		time = clock();
		result = fibonacciSequence(i);
		time = clock() - time;
		timeTaken = ((double)time) / CLOCKS_PER_SEC;

		fprintf(pFile, "%i,%f,%li\n", i, timeTaken, result);
		printf("%i takes %f seconds and the result is %li\n", i, timeTaken, result);
	}

	fclose(pFile);

	return 0;
}

long fibonacciSequence(long n)
{
	if (n == 1 || n == 2)
		return 1;
	else
		return fibonacciSequence(n - 1) + fibonacciSequence(n - 2);
}
