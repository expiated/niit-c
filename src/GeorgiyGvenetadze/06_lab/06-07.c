#include <stdio.h>
#include <windows.h>

#define HEIGHT 9
#define WIDTH 28
#define HERO 'X'
#define MARKED '*'
#define DEAD '-'

#define DEAD_END 0
#define LEFT 1
#define UP 2
#define RIGHT 3
#define DOWN 4

/*
�������� ���������, ������� ������� ����� �� ���������
���������:

(a) �������� ������� � ���� ���������� ����������� �������;
(b) ��������� ������� - � ������;
(c) ��������� ���������� (�� �������), ��� ��������� ������ ������� ����-
��;
(d) ���� ������ ��������, ��������� ������������ � ������ ����� � ��
�����������;
(e) ��������� ����� � ������ �� ��������� ��� ����������� ��� �������
�������.

���������:
�������� �#� ���������� ����� ���������
�������� ������ - ��������� ������
�������� �X� - �������������� ��������
*/

char maze[HEIGHT][WIDTH] =
{
	{ '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#' },
	{ '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#' },
	{ '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', ' ', ' ', '#', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#' },
	{ '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', '#', '#', '#', '#', '#', '#', '#', ' ', ' ', '#', '#', '#' },
	{ '#', ' ', '#', '#', '#', '#', '#', '#', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#' },
	{ '#', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', '#', '#', '#', '#', '#', '#', '#', ' ', ' ', ' ', '#', '#' },
	{ '#', '#', '#', '#', '#', ' ', ' ', '#', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', '#' },
	{ ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', '#', '#', '#', '#', '#' },
	{ '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#' }
};

char mapOfMaze[HEIGHT][WIDTH] = { 0 };

void draw();

int pathfinder(int row, int col);

void placeHero(int row, int col);

void markEraser();

int main()
{
	placeHero(HEIGHT / 2, WIDTH / 2);

	return 0;
}

void draw()
{
	for (int i = 0; i < HEIGHT; i++)
	{
		for (int j = 0; j < WIDTH; j++)
			putchar(maze[i][j]);
		putchar('\n');
	}
}

int pathfinder(int row, int col)
{
	if (maze[row - 1][col] != '#' && mapOfMaze[row - 1][col] != MARKED && mapOfMaze[row - 1][col] != DEAD)
		return UP;
	else if (maze[row + 1][col] != '#' && mapOfMaze[row + 1][col] != MARKED && mapOfMaze[row + 1][col] != DEAD)
		return DOWN;
	else if (maze[row][col - 1] != '#' && mapOfMaze[row][col - 1] != MARKED && mapOfMaze[row][col - 1] != DEAD)
		return LEFT;
	else if (maze[row][col + 1] != '#' && mapOfMaze[row][col + 1] != MARKED && mapOfMaze[row][col + 1] != DEAD)
		return RIGHT;
	else
		return 0;
}

void placeHero(int row, int col)
{
	maze[row][col] = HERO;
	draw();
	Sleep(200);
	system("cls");

	//if we already are at the exit
	if (maze[0][col] == HERO || maze[row][0] == HERO
		|| maze[HEIGHT - 1][col] == HERO || maze[row][WIDTH - 1] == HERO)
		printf("\n\nCongratulation! You escaped from the maze!\n\n");
	else
	{
		//here we choose the direction
		int direction = pathfinder(row, col);
		if (direction == UP)
		{
			maze[row][col] = ' ';
			mapOfMaze[row - 1][col] = MARKED;
			placeHero(row - 1, col);
		}
		else if (direction == DOWN)
		{
			maze[row][col] = ' ';
			mapOfMaze[row + 1][col] = MARKED;
			placeHero(row + 1, col);
		}
		else if (direction == LEFT)
		{
			maze[row][col] = ' ';
			mapOfMaze[row][col - 1] = MARKED;
			placeHero(row, col - 1);
		}
		else if (direction == RIGHT)
		{
			maze[row][col] = ' ';
			mapOfMaze[row][col + 1] = MARKED;
			placeHero(row, col + 1);
		}
		else if (direction == DEAD_END)
		{
			//if we are in the dead end - we mark this square as DEAD
			//and erase all the MARKED marks
			mapOfMaze[row][col] = DEAD;
			markEraser();
			placeHero(row, col);
		}
	}
}

void markEraser()
{
	for (int i = 0; i < HEIGHT; i++)
	{
		for (int j = 0; j < WIDTH; j++)
		if (mapOfMaze[i][j] != DEAD)
			mapOfMaze[i][j] = ' ';
	}
}