#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

#define OUT 0

/*
�������� ���������, ������� ��������� ������������� ���������
��������������� ���������, ��������� � ���� ��������� ������-
��� ������. ������������� ��������� 4-� �������� ��������. ��-
����� ���������� ������������ �������� ��������
���������:
� ������ ��������� ����� ����������� �������: 0-9,+,-,*,/,(,) ��������� ��-
��� ���� ��������, �� ���� ������� �������� �� ������ ����� 3, 8, � �����
���� ��������, ��������, ((6+8)*3) ��� (((7-1)/(4+2))-9). ������������-
��, ��� ������ � ��������� ������ ���������, �� ���� ���������� ��������
����� ���������� �������� � ��� �� ���������� ��������.
��������� ������ �������� �� ��������� �������:
(a) int main(int argc, char* argv[]) - ������� �������, � ������� ����������-
���� ����� ����������� ������� eval ��� ���������� ���������
(b) int eval(char *buf) - �������, ����������� ������, ������������ � buf
(c) char partition(char *buf, char *expr1, char *expr2) - �������, ������� ���-
������ ������, ������������ � buf �� ��� �����: ������ � ������ ���-
������, ���� �������� � ������ �� ������ ���������
*/

int eval(char *buf);

char partition(char *buf, char *expr1, char *expr2);

void spacesRemoval(char *buf);

void bracketsRemoval(char *buf);

int isnotclosed(char *buf);

int indexOfOperator(char ch, char *buf);

int findOperator(char *buf);

int main(int argc, char* argv[])
{
	if (argc != 2)
	{
		printf("Missed expression!\n");
		return 1;
	}
	if (isnotclosed(argv[1]))
	{
		printf("WRONG\n");
		return 1;
	}

	spacesRemoval(argv[1]);
	printf("The result of %s equals %d\n", argv[1], eval(argv[1]));

	return 0;
}

int eval(char *buf)
{
	char expr1[256] = { 0 };
	char expr2[256] = { 0 };

	if (isnotclosed(buf))
	{
		printf("%s - brackets quantity is different\n", buf);
		return 0;
	}

	bracketsRemoval(buf);
	int index = findOperator(buf);

	if (index == -1)
		return atoi(buf);

	partition(buf, expr1, expr2);

	if (buf[index] == '*')
		return eval(expr1) * eval(expr2);
	if (buf[index] == '/')
		return eval(expr1) / eval(expr2);
	if (buf[index] == '+')
		return eval(expr1) + eval(expr2);
	if (buf[index] == '-')
		return eval(expr1) - eval(expr2);
}

char partition(char *buf, char *expr1, char *expr2)
{
	int index = findOperator(buf);
	if (index > -1)
	{
		for (int i = 0; i < index; i++)
			expr1[i] = buf[i];
		for (int i = index + 1, j = 0; i < strlen(buf); i++, j++)
			expr2[j] = buf[i];
	}
	return buf[index];
}

void spacesRemoval(char *buf)
{
	for (int i = 0; i < strlen(buf); i++)
	if (isspace(buf[i]))
	{
		for (int j = i; j < strlen(buf); j++)
			buf[j] = buf[j + 1];
		i--;
	}
}

void bracketsRemoval(char *buf)
{
	int index = findOperator(buf);

	if (strlen(buf) > 1 && index == -1)
	{
		if (buf[0] == '(')
		for (int i = 0; i < strlen(buf); i++)
			buf[i] = buf[i + 1];
		if (buf[strlen(buf) - 1] == ')')
			buf[strlen(buf) - 1] = buf[strlen(buf)];
	}
}

int isnotclosed(char *buf)
{
	int counter = 0;
	for (int i = 0; i < strlen(buf); i++)
	{
		if (buf[i] == '(')
			counter++;
		if (buf[i] == ')')
			counter--;
	}
	return counter;
}

int indexOfOperator(char ch, char *buf)
{
	int index = -1;
	int condition = OUT;
	for (int i = 0; i < strlen(buf); i++)
	{
		if (buf[i] == '(') condition++;
		if (buf[i] == ')') condition--;
		if (buf[i] == ch && condition == OUT)
			return index = i;
	}
	return index;
}

int findOperator(char *buf)
{
	int index = -1;
	if ((index = indexOfOperator('-', buf)) != -1) return index;
	if ((index = indexOfOperator('+', buf)) != -1) return index;
	if ((index = indexOfOperator('/', buf)) != -1) return index;
	if ((index = indexOfOperator('*', buf)) != -1) return index;
	return index;
}