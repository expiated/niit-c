#include <stdio.h>

/*
�������� ���������� ����������� �������, ����������� n-��
������� ���� ���������, �� ��� ��������������� �������� �����-
���
���������:
����� ������� ��� �������: ���� ���������� ��������������� �� main � ��-
������ ������, ���������������, ������� � �������� �����������
*/

long fibonacci(long i, long j, long n);

long fibonacciWrapper(long n);

int main()
{
	long userNum = 0;
	
	printf("Enter the Fibonacci element you want to see: ");
	scanf_s("%li", &userNum);

	printf("%li\n", fibonacciWrapper(userNum));

	return 0;
}

long fibonacci(long i, long j, long n)
{
	if (n == 1)
		return j;
	else
		return fibonacci(j, j + i, n - 1);
}

long fibonacciWrapper(long n)
{
	long result = 0;
	for (int i = 0; i < n; i++)
		result = fibonacci(0, 1, n);
	return result;
}