#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

/*
�������� ���������, ������� ��������� ������ ������������ �
����������� ���������
���������:
��������� ��������� ��������� ������������������ ��������:
(a) ��������� �� ��������� ������ �������� ������� ������ M;
(b) ������� ������ ������������� ������� N = 2M;
(c) �������� ������ ��� ������������ ������;
(d) ��������� ������� ��������� ������ �������;
(e) ������� ����� ������������ � ������������ ��������;
(f) ���������� ����� ���������� ������������ ������������ � ��������-
��� ��������;
(g) ����������� ������������ ������
*/

void randomFiller(int arr[], int sizeOfArr);

unsigned long traditionalAmountFinder(int arr[], int sizeOfArr);

unsigned long recursionAmountFinder(int *arr, int sizeOfArr);

int main()
{
	srand(time(NULL));

	int powOfTwo = 0;
	size_t size = 0;
	clock_t time;
	double tradFuncTimeTaken;
	double recFuncTimeTaken;
	unsigned long result;

	printf("Enter any number: ");
	scanf_s("%i", &powOfTwo);
	size = (int)pow(2, powOfTwo);

	int *arr = malloc(size * sizeof(int));

	randomFiller(arr, size);

	time = clock();
	result = traditionalAmountFinder(arr, size);
	time = clock() - time;
	tradFuncTimeTaken = ((double)time) / CLOCKS_PER_SEC;

	printf("The result of the traditional execution is %li and it takes %f\n", result, tradFuncTimeTaken);

	time = clock();
	result = recursionAmountFinder(arr, size - 1);
	time = clock() - time;
	recFuncTimeTaken = ((double)time) / CLOCKS_PER_SEC;

	printf("The result of the traditional execution is %li and it takes %f\n", result, recFuncTimeTaken);

	free(arr);

	return 0;
}

void randomFiller(int arr[], int sizeOfArr)
{
	for (int i = 0; i < sizeOfArr; i++)
		arr[i] = rand() % 1000;
}

unsigned long traditionalAmountFinder(int arr[], int sizeOfArr)
{
	unsigned long amount = 0;
	for (int i = 0; i < sizeOfArr; i++)
		amount += arr[i];

	return amount;
}

unsigned long recursionAmountFinder(int *arr, int sizeOfArr)
{
	if (sizeOfArr == 0)
		return *(arr + sizeOfArr);
	else
		return *(arr + sizeOfArr) + recursionAmountFinder(arr, sizeOfArr - 1);
}